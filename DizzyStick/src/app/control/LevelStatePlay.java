package app.control;

public interface LevelStatePlay {
	public void controlRotate();

	public void buttonPlayEvent();

	public void ButtonOption();

	public void checkFinish();

	public void countNum();

	public void timeControl();

	public void createDialogComplete();

	public void createDialogMain();

	public void createDialogDizzyStick();

	public void createDialogBestScore();

	public void createDialogGameOver();

	public void createMp();

	public void soundChecker();

	public void writeBestScore();

	public void readBestScore();
}
