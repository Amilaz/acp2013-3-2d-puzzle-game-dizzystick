package app.dizzystick;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import app.control.OrientationManager;

public class MainActivity extends Activity {

	public Boolean soundStatus;
	protected Dialog optionDialog;
	protected Button onButton, offButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);

		OrientationManager om = new OrientationManager(MainActivity.this);
		om.disable();
		om.disableRotation();

		new CountDownTimer(1000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {

			}

			@Override
			public void onFinish() {
				createDialogOption();
			}
		}.start();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onStop() {
		System.exit(0);
	}

	public void onBackPressed() {
		System.exit(0);
	}

	@SuppressWarnings("static-access")
	public void createDialogOption() {
		optionDialog = new Dialog(MainActivity.this);
		optionDialog
				.requestWindowFeature(optionDialog.getWindow().FEATURE_NO_TITLE);
		optionDialog.setContentView(R.layout.dialog);
		optionDialog.setCancelable(false);

		onButton = (Button) optionDialog.findViewById(R.id.bi_dialog);
		onButton.setText("On");
		onButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				soundStatus = true;
				sendValueIntent();
			}
		});

		offButton = (Button) optionDialog.findViewById(R.id.bt_dialog);
		offButton.setText("Off");
		offButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				soundStatus = false;
				sendValueIntent();
			}
		});

		TextView textView1 = (TextView) optionDialog
				.findViewById(R.id.textview1_dialog);
		textView1.setText("Sound");

		optionDialog.show();
	}

	public void sendValueIntent() {
		Intent i = new Intent(getApplicationContext(), LaunchActivity.class);
		i.putExtra("SoundStatus", soundStatus);
		startActivity(i);
		System.exit(0);
	}

}
