package app.dizzystick;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import app.control.OrientationManager;

public class HowToPlayActivity extends Activity {

	protected MediaPlayer mp;
	protected Boolean soundStatus;
	protected Button mainButton, nextButton;
	protected ImageView infoView;
	protected int i = 1;
	protected int[] imageID = { R.drawable.htp_info_1, R.drawable.htp_info_2,
			R.drawable.htp_info_3, R.drawable.htp_info_4};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_how_to_play);
		
		soundStatus = getIntent().getExtras().getBoolean("SoundStatus");

		OrientationManager om = new OrientationManager(HowToPlayActivity.this);
		om.disable();
		om.disableRotationLandscape();
		createMp();
		soundChecker();
		infoView = (ImageView) findViewById(R.id.htp_image);
		buttonEvent();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.how_to_play, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		System.exit(0);
	}

	public void buttonEvent() {
		mainButton = (Button) findViewById(R.id.main_button_htp);
		mainButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				System.exit(0);
			}
		});

		nextButton = (Button) findViewById(R.id.next_buton_htp);
		nextButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				infoView.setImageResource(imageID[i]);
				i++;
				if (i == 4) {
					i = 0;
				}
			}
		});
	}

	public void createMp() {
		mp = MediaPlayer.create(HowToPlayActivity.this, R.raw.music_1);
		mp.setLooping(true);
	}

	public void soundChecker() {
		if (soundStatus) {
			mp.start();
		} else {
			mp.pause();
		}
	}

}
