package app.dizzystick;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import app.control.OrientationManager;

public class MenuActivity extends Activity {

	protected Button playBuuton, htpButton, optionButton, exitButton;
	protected Dialog optionDialog, exitDialog;
	protected Button noButton, yesButton;
	protected Button onButton, offButton;
	protected Boolean soundStatus;
	protected MediaPlayer mp;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_menu);

		OrientationManager om = new OrientationManager(MenuActivity.this);
		om.disable();
		om.disableRotationLandscape();
		soundStatus = getIntent().getExtras().getBoolean("SoundStatus");
		createButton();
		createMp();

	}

	@Override
	public void onBackPressed() {
		createDialogExitBox();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	public void createButton() {
		playBuuton = (Button) findViewById(R.id.play_button_main);
		playBuuton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						SelectLevelActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
			}
		});

		htpButton = (Button) findViewById(R.id.htp_button_main);
		htpButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						HowToPlayActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
			}
		});

		optionButton = (Button) findViewById(R.id.option_button_main);
		optionButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialogOption();
			}
		});

		exitButton = (Button) findViewById(R.id.exit_button_main);
		exitButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialogExitBox();
			}
		});
	}

	@SuppressWarnings({ "static-access" })
	private void createDialogExitBox() {
		exitDialog = new Dialog(MenuActivity.this);
		exitDialog
				.requestWindowFeature(exitDialog.getWindow().FEATURE_NO_TITLE);
		exitDialog.setContentView(R.layout.dialog_2);
		exitDialog.setCancelable(true);

		yesButton = (Button) exitDialog.findViewById(R.id.bi_dialog);
		yesButton.setText("Yes");
		yesButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				System.exit(0);
			}
		});

		noButton = (Button) exitDialog.findViewById(R.id.bt_dialog);
		noButton.setText("No");
		noButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				exitDialog.cancel();
			}
		});

		TextView textView1 = (TextView) exitDialog
				.findViewById(R.id.textview1_dialog);
		textView1.setText("Do you want to exit?");

		exitDialog.show();
	}

	@SuppressWarnings("static-access")
	public void createDialogOption() {
		optionDialog = new Dialog(MenuActivity.this);
		optionDialog
				.requestWindowFeature(optionDialog.getWindow().FEATURE_NO_TITLE);
		optionDialog.setContentView(R.layout.dialog);
		optionDialog.setCancelable(true);

		onButton = (Button) optionDialog.findViewById(R.id.bi_dialog);
		onButton.setText("On");
		onButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				soundStatus = true;
				soundChecker();
				optionDialog.cancel();
			}
		});

		offButton = (Button) optionDialog.findViewById(R.id.bt_dialog);
		offButton.setText("Off");
		offButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				soundStatus = false;
				soundChecker();
				optionDialog.cancel();
			}
		});

		TextView textView1 = (TextView) optionDialog
				.findViewById(R.id.textview1_dialog);
		textView1.setText("Sound");

		optionDialog.show();
	}

	public void onStop() {
		super.onStop();
		mp.stop();
	}

	public void createMp() {
		mp = MediaPlayer.create(MenuActivity.this, R.raw.music_1);
		mp.setLooping(true);
		soundChecker();
	}

	public void soundChecker() {
		if (soundStatus) {
			mp = MediaPlayer.create(MenuActivity.this, R.raw.music_1);
			mp.start();
		} else {
			mp.pause();
		}
	}

	public void onResume() {
		super.onResume();
		mp.start();
	}

	public void onDestroy() {
		super.onDestroy();
		mp.stop();
		mp.release();
		mp = null;
	}

}
