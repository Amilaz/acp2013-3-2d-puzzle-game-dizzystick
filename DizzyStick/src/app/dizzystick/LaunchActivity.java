package app.dizzystick;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import app.control.OrientationManager;

public class LaunchActivity extends Activity {

	protected boolean soundStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_launch);
		OrientationManager om = new OrientationManager(LaunchActivity.this);
		om.disable();
		om.disableRotationLandscape();
		soundStatus = getIntent().getExtras().getBoolean("SoundStatus");
		new CountDownTimer(3000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {

			}

			@Override
			public void onFinish() {
				Intent i = new Intent(getApplicationContext(),
						MenuActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		}.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.launch, menu);
		return true;
	}

}
