package app.dizzystick;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import app.control.OrientationManager;

public class MoreActivity extends Activity {

	protected MediaPlayer mp;
	protected Boolean soundStatus;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_more);
		
		OrientationManager om = new OrientationManager(MoreActivity.this);
		om.disable();
		om.disableRotationLandscape();

		soundStatus = getIntent().getExtras().getBoolean("SoundStatus");
		createMp();
		soundChecker();

		new CountDownTimer(3000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {

			}

			@Override
			public void onFinish() {
				Intent i = new Intent(getApplicationContext(),
						CreditActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				System.exit(0);
			}
		}.start();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.more, menu);
		return true;
	}

	@Override
	public void onBackPressed() {

	}

	public void createMp() {
		// TODO Auto-generated method stub
		mp = MediaPlayer.create(MoreActivity.this, R.raw.music_1);
		mp.setLooping(true);
	}

	public void soundChecker() {
		// TODO Auto-generated method stub
		if (soundStatus) {
			mp.start();
		} else {
			mp.pause();
		}
	}

}
