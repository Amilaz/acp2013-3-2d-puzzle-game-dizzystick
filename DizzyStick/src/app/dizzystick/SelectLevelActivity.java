package app.dizzystick;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import app.control.OrientationManager;
import app.level.LevelIActivity;
import app.level.LevelIIActivity;
import app.level.LevelIIIActivity;
import app.level.LevelIVActivity;
import app.level.LevelVActivity;

public class SelectLevelActivity extends Activity {

	protected MediaPlayer mp;
	public Boolean soundStatus;
	protected Button lv1, lv2, lv3, lv4, lv5;
	protected Button main;
	final static int Num_FILE = 5;
	final static String[] FILE_NAME = { "lv1-score", "lv2-score", "lv3-score",
			"lv4-score", "lv5-score" };
	protected boolean[] stateStatus = new boolean[Num_FILE];
	protected SharedPreferences stringShared;
	protected SharedPreferences.Editor edit;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_select_level);

		soundStatus = getIntent().getExtras().getBoolean("SoundStatus");

		OrientationManager om = new OrientationManager(SelectLevelActivity.this);
		om.disable();
		createMp();
		soundChecker();
		om.disableRotationLandscape();
		checkStateStatus();
		createButton();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_level, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		System.exit(0);
	}

	public void createButton() {
		lv1 = (Button) findViewById(R.id.level1_button);
		lv1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelIActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		lv2 = (Button) findViewById(R.id.level2_button);
		if (!stateStatus[0]) {
			lv2.setEnabled(false);
		}
		lv2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelIIActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		lv3 = (Button) findViewById(R.id.level3_button);
		if (!stateStatus[1]) {
			lv3.setEnabled(false);
		}
		lv3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelIIIActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		lv4 = (Button) findViewById(R.id.level4_button);
		if (!stateStatus[2]) {
			lv4.setEnabled(false);
		}
		lv4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelIVActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		lv5 = (Button) findViewById(R.id.level5_button);
		if (!stateStatus[3]) {
			lv5.setEnabled(false);
		}
		lv5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelVActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		main = (Button) findViewById(R.id.main_button_select);
		main.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				System.exit(0);
			}
		});
	}

	public void checkStateStatus() {
		for (int i = 0; i < Num_FILE; i++) {
			try {
				stringShared = this.getSharedPreferences(FILE_NAME[i],
						MODE_APPEND);
				edit = stringShared.edit();
				stateStatus[i] = stringShared.getBoolean("statestatus", false);
			} catch (Exception ex) {
				stateStatus[i] = false;
			}
		}
	}

	public void createMp() {
		// TODO Auto-generated method stub
		mp = MediaPlayer.create(SelectLevelActivity.this, R.raw.music_1);
		mp.setLooping(true);
	}

	public void soundChecker() {
		// TODO Auto-generated method stub
		if (soundStatus) {
			mp.start();
		} else {
			mp.pause();
		}
	}

}
