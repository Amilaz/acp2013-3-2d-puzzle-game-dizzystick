package app.level;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import app.control.LevelStatePlay;
import app.control.OrientationManager;
import app.dizzystick.CreditActivity;
import app.dizzystick.MenuActivity;
import app.dizzystick.R;

public class LevelSpecialIActivity extends Activity implements LevelStatePlay {

	protected Boolean soundStatus;
	protected CountDownTimer timeBack;
	protected Dialog complete, mainMenu, dizzy, score, gameover;
	protected Button dz, main, bestScore, numClick, time;
	protected ImageButton b0, b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11,
			b12, b13, b14, b15, b16, b17;
	protected Boolean[] buttonState = { false, true, true, false, false, true,
			true, false, true, true, false, false, false, true, false, true,
			true, false };
	protected int[] buttonStateImage = { R.drawable.button_state1,
			R.drawable.button_state2 };
	protected Boolean completeGame;
	protected int numCount = 0;
	protected int numTime = 100;
	protected CountDownTimer timer;
	protected MediaPlayer mp;
	protected OrientationManager om;
	protected int bestClick, bestTime;
	protected boolean stateStatus, stateStatus2;
	protected SharedPreferences stringShared;
	protected SharedPreferences.Editor edit;
	final static String FILE_NAME = "lvsi-score";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_level_special_i);
		soundStatus = getIntent().getExtras().getBoolean("SoundStatus");
		readBestScore();
		controlRotate();
		createMp();
		soundChecker();
		buttonPlayEvent();
		ButtonOption();
		timeControl();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.level_special_i, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		System.exit(0);
	}

	public void onResume() {
		super.onResume();
		mp.start();
	}

	public void onDestroy() {
		super.onDestroy();
		mp.stop();
		mp.release();
		mp = null;
	}

	public void onPause() {
		super.onPause();
		mp.pause();
	}

	public void controlRotate() {
		// TODO Auto-generated method stub
		om = new OrientationManager(LevelSpecialIActivity.this);
		om.disable();
		om.disableRotationLandscape();
	}

	@Override
	public void buttonPlayEvent() {
		// TODO Auto-generated method stub
		b0 = (ImageButton) findViewById(R.id.play_button1_lvsi);
		b0.setImageResource(buttonStateImage[0]);
		b0.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[0]) {
					b0.setImageResource(buttonStateImage[0]);
					buttonState[0] = false;
				} else {
					b0.setImageResource(buttonStateImage[1]);
					buttonState[0] = true;
				}
				if (buttonState[1]) {
					b1.setImageResource(buttonStateImage[0]);
					buttonState[1] = false;
				} else {
					b1.setImageResource(buttonStateImage[1]);
					buttonState[1] = true;
				}
				checkFinish();
			}
		});

		b1 = (ImageButton) findViewById(R.id.play_button2_lvsi);
		b1.setImageResource(buttonStateImage[1]);
		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[0]) {
					b0.setImageResource(buttonStateImage[0]);
					buttonState[0] = false;
				} else {
					b0.setImageResource(buttonStateImage[1]);
					buttonState[0] = true;
				}
				if (buttonState[1]) {
					b1.setImageResource(buttonStateImage[0]);
					buttonState[1] = false;
				} else {
					b1.setImageResource(buttonStateImage[1]);
					buttonState[1] = true;
				}
				if (buttonState[2]) {
					b2.setImageResource(buttonStateImage[0]);
					buttonState[2] = false;
				} else {
					b2.setImageResource(buttonStateImage[1]);
					buttonState[2] = true;

				}
				checkFinish();
			}
		});

		b2 = (ImageButton) findViewById(R.id.play_button3_lvsi);
		b2.setImageResource(buttonStateImage[1]);
		b2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[1]) {
					b1.setImageResource(buttonStateImage[0]);
					buttonState[1] = false;

				} else {
					b1.setImageResource(buttonStateImage[1]);
					buttonState[1] = true;
				}
				if (buttonState[2]) {
					b2.setImageResource(buttonStateImage[0]);
					buttonState[2] = false;
				} else {
					b2.setImageResource(buttonStateImage[1]);
					buttonState[2] = true;
				}
				if (buttonState[3]) {
					b3.setImageResource(buttonStateImage[0]);
					buttonState[3] = false;

				} else {
					b3.setImageResource(buttonStateImage[1]);
					buttonState[3] = true;
				}
				checkFinish();
			}
		});

		b3 = (ImageButton) findViewById(R.id.play_button4_lvsi);
		b3.setImageResource(buttonStateImage[0]);
		b3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[2]) {
					b2.setImageResource(buttonStateImage[0]);
					buttonState[2] = false;
				} else {
					b2.setImageResource(buttonStateImage[1]);
					buttonState[2] = true;
				}
				if (buttonState[3]) {
					b3.setImageResource(buttonStateImage[0]);
					buttonState[3] = false;
				} else {
					b3.setImageResource(buttonStateImage[1]);
					buttonState[3] = true;
				}
				if (buttonState[4]) {
					b4.setImageResource(buttonStateImage[0]);
					buttonState[4] = false;
				} else {
					b4.setImageResource(buttonStateImage[1]);
					buttonState[4] = true;
				}
				checkFinish();
			}
		});

		b4 = (ImageButton) findViewById(R.id.play_button5_lvsi);
		b4.setImageResource(buttonStateImage[0]);
		b4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[3]) {
					b3.setImageResource(buttonStateImage[0]);
					buttonState[3] = false;
				} else {
					b3.setImageResource(buttonStateImage[1]);
					buttonState[3] = true;
				}
				if (buttonState[4]) {
					b4.setImageResource(buttonStateImage[0]);
					buttonState[4] = false;
				} else {
					b4.setImageResource(buttonStateImage[1]);
					buttonState[4] = true;
				}
				if (buttonState[5]) {
					b5.setImageResource(buttonStateImage[0]);
					buttonState[5] = false;

				} else {
					b5.setImageResource(buttonStateImage[1]);
					buttonState[5] = true;
				}
				checkFinish();
			}
		});

		b5 = (ImageButton) findViewById(R.id.play_button6_lvsi);
		b5.setImageResource(buttonStateImage[1]);
		b5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[4]) {
					b4.setImageResource(buttonStateImage[0]);
					buttonState[4] = false;
				} else {
					b4.setImageResource(buttonStateImage[1]);
					buttonState[4] = true;
				}
				if (buttonState[5]) {
					b5.setImageResource(buttonStateImage[0]);
					buttonState[5] = false;
				} else {
					b5.setImageResource(buttonStateImage[1]);
					buttonState[5] = true;
				}
				if (buttonState[6]) {
					b6.setImageResource(buttonStateImage[0]);
					buttonState[6] = false;

				} else {
					b6.setImageResource(buttonStateImage[1]);
					buttonState[6] = true;
				}
				checkFinish();
			}
		});

		b6 = (ImageButton) findViewById(R.id.play_button7_lvsi);
		b6.setImageResource(buttonStateImage[1]);
		b6.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[5]) {
					b5.setImageResource(buttonStateImage[0]);
					buttonState[5] = false;
				} else {
					b5.setImageResource(buttonStateImage[1]);
					buttonState[5] = true;
				}
				if (buttonState[6]) {
					b6.setImageResource(buttonStateImage[0]);
					buttonState[6] = false;
				} else {
					b6.setImageResource(buttonStateImage[1]);
					buttonState[6] = true;
				}
				if (buttonState[7]) {
					b7.setImageResource(buttonStateImage[0]);
					buttonState[7] = false;

				} else {
					b7.setImageResource(buttonStateImage[1]);
					buttonState[7] = true;
				}
				checkFinish();
			}
		});

		b7 = (ImageButton) findViewById(R.id.play_button8_lvsi);
		b7.setImageResource(buttonStateImage[0]);
		b7.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[6]) {
					b6.setImageResource(buttonStateImage[0]);
					buttonState[6] = false;
				} else {
					b6.setImageResource(buttonStateImage[1]);
					buttonState[6] = true;
				}
				if (buttonState[7]) {
					b7.setImageResource(buttonStateImage[0]);
					buttonState[7] = false;
				} else {
					b7.setImageResource(buttonStateImage[1]);
					buttonState[7] = true;
				}
				if (buttonState[8]) {
					b8.setImageResource(buttonStateImage[0]);
					buttonState[8] = false;

				} else {
					b8.setImageResource(buttonStateImage[1]);
					buttonState[8] = true;
				}
				checkFinish();
			}
		});

		b8 = (ImageButton) findViewById(R.id.play_button9_lvsi);
		b8.setImageResource(buttonStateImage[1]);
		b8.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[7]) {
					b7.setImageResource(buttonStateImage[0]);
					buttonState[7] = false;
				} else {
					b7.setImageResource(buttonStateImage[1]);
					buttonState[7] = true;
				}
				if (buttonState[8]) {
					b8.setImageResource(buttonStateImage[0]);
					buttonState[8] = false;
				} else {
					b8.setImageResource(buttonStateImage[1]);
					buttonState[8] = true;
				}
				if (buttonState[9]) {
					b9.setImageResource(buttonStateImage[0]);
					buttonState[9] = false;

				} else {
					b9.setImageResource(buttonStateImage[1]);
					buttonState[9] = true;
				}
				checkFinish();
			}
		});

		b9 = (ImageButton) findViewById(R.id.play_button10_lvsi);
		b9.setImageResource(buttonStateImage[1]);
		b9.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[8]) {
					b8.setImageResource(buttonStateImage[0]);
					buttonState[8] = false;
				} else {
					b8.setImageResource(buttonStateImage[1]);
					buttonState[8] = true;
				}
				if (buttonState[9]) {
					b9.setImageResource(buttonStateImage[0]);
					buttonState[9] = false;
				} else {
					b9.setImageResource(buttonStateImage[1]);
					buttonState[9] = true;
				}
				if (buttonState[10]) {
					b10.setImageResource(buttonStateImage[0]);
					buttonState[10] = false;

				} else {
					b10.setImageResource(buttonStateImage[1]);
					buttonState[10] = true;
				}
				checkFinish();
			}
		});
		
		b10 = (ImageButton) findViewById(R.id.play_button11_lvsi);
		b10.setImageResource(buttonStateImage[0]);
		b10.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[9]) {
					b9.setImageResource(buttonStateImage[0]);
					buttonState[9] = false;
				} else {
					b9.setImageResource(buttonStateImage[1]);
					buttonState[9] = true;
				}
				if (buttonState[10]) {
					b10.setImageResource(buttonStateImage[0]);
					buttonState[10] = false;
				} else {
					b10.setImageResource(buttonStateImage[1]);
					buttonState[10] = true;
				}
				if (buttonState[11]) {
					b11.setImageResource(buttonStateImage[0]);
					buttonState[11] = false;

				} else {
					b11.setImageResource(buttonStateImage[1]);
					buttonState[11] = true;
				}
				checkFinish();
			}
		});
		
		b11 = (ImageButton) findViewById(R.id.play_button12_lvsi);
		b11.setImageResource(buttonStateImage[0]);
		b11.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[10]) {
					b10.setImageResource(buttonStateImage[0]);
					buttonState[10] = false;
				} else {
					b10.setImageResource(buttonStateImage[1]);
					buttonState[10] = true;
				}
				if (buttonState[11]) {
					b11.setImageResource(buttonStateImage[0]);
					buttonState[11] = false;
				} else {
					b11.setImageResource(buttonStateImage[1]);
					buttonState[11] = true;
				}
				if (buttonState[12]) {
					b12.setImageResource(buttonStateImage[0]);
					buttonState[12] = false;

				} else {
					b12.setImageResource(buttonStateImage[1]);
					buttonState[12] = true;
				}
				checkFinish();
			}
		});
		
		b12 = (ImageButton) findViewById(R.id.play_button13_lvsi);
		b12.setImageResource(buttonStateImage[0]);
		b12.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[11]) {
					b11.setImageResource(buttonStateImage[0]);
					buttonState[11] = false;
				} else {
					b11.setImageResource(buttonStateImage[1]);
					buttonState[11] = true;
				}
				if (buttonState[12]) {
					b12.setImageResource(buttonStateImage[0]);
					buttonState[12] = false;
				} else {
					b12.setImageResource(buttonStateImage[1]);
					buttonState[12] = true;
				}
				if (buttonState[13]) {
					b13.setImageResource(buttonStateImage[0]);
					buttonState[13] = false;

				} else {
					b13.setImageResource(buttonStateImage[1]);
					buttonState[13] = true;
				}
				checkFinish();
			}
		});
		
		b13 = (ImageButton) findViewById(R.id.play_button14_lvsi);
		b13.setImageResource(buttonStateImage[1]);
		b13.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[12]) {
					b12.setImageResource(buttonStateImage[0]);
					buttonState[12] = false;
				} else {
					b12.setImageResource(buttonStateImage[1]);
					buttonState[12] = true;
				}
				if (buttonState[13]) {
					b13.setImageResource(buttonStateImage[0]);
					buttonState[13] = false;
				} else {
					b13.setImageResource(buttonStateImage[1]);
					buttonState[9] = true;
				}
				if (buttonState[14]) {
					b14.setImageResource(buttonStateImage[0]);
					buttonState[14] = false;

				} else {
					b14.setImageResource(buttonStateImage[1]);
					buttonState[14] = true;
				}
				checkFinish();
			}
		});
		
		b14 = (ImageButton) findViewById(R.id.play_button15_lvsi);
		b14.setImageResource(buttonStateImage[0]);
		b14.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[13]) {
					b13.setImageResource(buttonStateImage[0]);
					buttonState[13] = false;
				} else {
					b13.setImageResource(buttonStateImage[1]);
					buttonState[13] = true;
				}
				if (buttonState[14]) {
					b14.setImageResource(buttonStateImage[0]);
					buttonState[14] = false;
				} else {
					b14.setImageResource(buttonStateImage[1]);
					buttonState[14] = true;
				}
				if (buttonState[15]) {
					b15.setImageResource(buttonStateImage[0]);
					buttonState[15] = false;

				} else {
					b15.setImageResource(buttonStateImage[1]);
					buttonState[15] = true;
				}
				checkFinish();
			}
		});
		
		b15 = (ImageButton) findViewById(R.id.play_button16_lvsi);
		b15.setImageResource(buttonStateImage[1]);
		b15.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[14]) {
					b14.setImageResource(buttonStateImage[0]);
					buttonState[14] = false;
				} else {
					b14.setImageResource(buttonStateImage[1]);
					buttonState[14] = true;
				}
				if (buttonState[15]) {
					b15.setImageResource(buttonStateImage[0]);
					buttonState[15] = false;
				} else {
					b15.setImageResource(buttonStateImage[1]);
					buttonState[15] = true;
				}
				if (buttonState[16]) {
					b16.setImageResource(buttonStateImage[0]);
					buttonState[16] = false;

				} else {
					b16.setImageResource(buttonStateImage[1]);
					buttonState[16] = true;
				}
				checkFinish();
			}
		});
		
		b16 = (ImageButton) findViewById(R.id.play_button17_lvsi);
		b16.setImageResource(buttonStateImage[1]);
		b16.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[15]) {
					b15.setImageResource(buttonStateImage[0]);
					buttonState[15] = false;
				} else {
					b15.setImageResource(buttonStateImage[1]);
					buttonState[15] = true;
				}
				if (buttonState[16]) {
					b16.setImageResource(buttonStateImage[0]);
					buttonState[16] = false;
				} else {
					b16.setImageResource(buttonStateImage[1]);
					buttonState[16] = true;
				}
				if (buttonState[17]) {
					b17.setImageResource(buttonStateImage[0]);
					buttonState[17] = false;

				} else {
					b17.setImageResource(buttonStateImage[1]);
					buttonState[17] = true;
				}
				checkFinish();
			}
		});

		b17 = (ImageButton) findViewById(R.id.play_button18_lvsi);
		b17.setImageResource(buttonStateImage[0]);
		b17.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countNum();
				if (buttonState[16]) {
					b16.setImageResource(buttonStateImage[0]);
					buttonState[16] = false;
				} else {
					b16.setImageResource(buttonStateImage[1]);
					buttonState[16] = true;
				}

				if (buttonState[17]) {
					b17.setImageResource(buttonStateImage[0]);
					buttonState[17] = false;
				} else {
					b17.setImageResource(buttonStateImage[1]);
					buttonState[17] = true;
				}
				checkFinish();
			}
		});

	}

	@Override
	public void ButtonOption() {
		// TODO Auto-generated method stub
		dz = (Button) findViewById(R.id.dz_button_lvsi);
		dz.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialogDizzyStick();
			}
		});

		main = (Button) findViewById(R.id.main_lvsi);
		main.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialogMain();
			}
		});

		bestScore = (Button) findViewById(R.id.best_score_lvsi);
		bestScore.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				createDialogBestScore();
			}
		});

		numClick = (Button) findViewById(R.id.num_button_lvsi);

		time = (Button) findViewById(R.id.time_button_lvsi);
	}

	@Override
	public void checkFinish() {
		// TODO Auto-generated method stub
		for (int i = 0; i < buttonState.length; i++) {
			completeGame = true;
			if (!buttonState[i]) {
				completeGame = false;
				break;
			}
		}
		if (completeGame) {
			timeBack.cancel();
			writeBestScore();
			createDialogComplete();
		}
	}

	@Override
	public void countNum() {
		// TODO Auto-generated method stub
		numCount++;
		numClick.setText(String.valueOf(numCount));
	}

	@Override
	public void timeControl() {
		// TODO Auto-generated method stub
		timeBack = new CountDownTimer(100000, 1000) {

			@Override
			public void onTick(long millisUntilFinished) {
				numTime -= 1;
				time.setText(String.valueOf(numTime));
			}

			@Override
			public void onFinish() {
				createDialogGameOver();
			}
		}.start();
	}

	@SuppressWarnings("static-access")
	@Override
	public void createDialogComplete() {
		// TODO Auto-generated method stub
		complete = new Dialog(LevelSpecialIActivity.this);
		complete.requestWindowFeature(complete.getWindow().FEATURE_NO_TITLE);
		complete.setContentView(R.layout.dialog_2);
		complete.setCancelable(false);

		Button restartButton = (Button) complete.findViewById(R.id.bi_dialog);
		restartButton.setText("Restart");
		restartButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelSpecialIActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		Button nextButton = (Button) complete.findViewById(R.id.bt_dialog);
		nextButton.setText("Next");
		nextButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
					Intent i = new Intent(getApplicationContext(),
							CreditActivity.class);
					i.putExtra("SoundStatus", soundStatus);
					startActivity(i);
					System.exit(0);
			}
		});

		TextView textView1 = (TextView) complete
				.findViewById(R.id.textview1_dialog);
		int timeScore = 100 - Integer.parseInt((String) time.getText());
		String score = "Your click : " + numClick.getText()
				+ " Time\n Your time : " + String.valueOf(timeScore) + " Sec";
		textView1.setText(score);

		complete.show();
	}

	@SuppressWarnings("static-access")
	@Override
	public void createDialogMain() {
		// TODO Auto-generated method stub
		mainMenu = new Dialog(LevelSpecialIActivity.this);
		mainMenu.requestWindowFeature(mainMenu.getWindow().FEATURE_NO_TITLE);
		mainMenu.setContentView(R.layout.dialog_2);
		mainMenu.setCancelable(true);

		Button yesButton = (Button) mainMenu.findViewById(R.id.bi_dialog);
		yesButton.setText("Yes");
		yesButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						MenuActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				System.exit(0);
			}
		});

		Button noButton = (Button) mainMenu.findViewById(R.id.bt_dialog);
		noButton.setText("No");
		noButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				mainMenu.cancel();
			}
		});

		TextView textView1 = (TextView) mainMenu
				.findViewById(R.id.textview1_dialog);
		textView1.setText("Do you want to Menu?");

		mainMenu.show();
	}

	@SuppressWarnings("static-access")
	@Override
	public void createDialogDizzyStick() {
		// TODO Auto-generated method stub
		dizzy = new Dialog(LevelSpecialIActivity.this);
		dizzy.requestWindowFeature(dizzy.getWindow().FEATURE_NO_TITLE);
		dizzy.setContentView(R.layout.dialog_1);
		dizzy.setCancelable(true);

		Button okButton = (Button) dizzy.findViewById(R.id.bh_dialog);
		okButton.setText("OK");
		okButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dizzy.cancel();
			}
		});

		TextView textView1 = (TextView) dizzy
				.findViewById(R.id.textview1_dialog_1);
		String info = "  Dizzy Stick" + "\nVersion (Beta)";
		textView1.setText(info);

		dizzy.show();
	}

	@SuppressWarnings("static-access")
	@Override
	public void createDialogBestScore() {
		// TODO Auto-generated method stub
		score = new Dialog(LevelSpecialIActivity.this);
		score.requestWindowFeature(score.getWindow().FEATURE_NO_TITLE);
		score.setContentView(R.layout.dialog_1);
		score.setCancelable(true);

		Button okButton = (Button) score.findViewById(R.id.bh_dialog);
		okButton.setText("OK");
		okButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				score.cancel();
			}
		});

		TextView textView1 = (TextView) score
				.findViewById(R.id.textview1_dialog_1);
		String scoreString = "Best Click : " + bestClick
				+ " Time\nBest Time : " + bestTime + " Sec";
		textView1.setText(scoreString);

		score.show();
	}

	@SuppressWarnings("static-access")
	@Override
	public void createDialogGameOver() {
		// TODO Auto-generated method stub
		gameover = new Dialog(LevelSpecialIActivity.this);
		gameover.requestWindowFeature(gameover.getWindow().FEATURE_NO_TITLE);
		gameover.setContentView(R.layout.dialog_1);
		gameover.setCancelable(false);

		Button restartButton = (Button) gameover.findViewById(R.id.bh_dialog);
		restartButton.setText("Restart");
		restartButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent i = new Intent(getApplicationContext(),
						LevelSpecialIActivity.class);
				i.putExtra("SoundStatus", soundStatus);
				startActivity(i);
				System.exit(0);
			}
		});

		TextView textView1 = (TextView) gameover
				.findViewById(R.id.textview1_dialog_1);
		textView1.setText("Game Over");

		gameover.show();
	}

	@Override
	public void createMp() {
		// TODO Auto-generated method stub
		mp = MediaPlayer.create(LevelSpecialIActivity.this, R.raw.music_1);
		mp.setLooping(true);
	}

	@Override
	public void soundChecker() {
		// TODO Auto-generated method stub
		if (soundStatus) {
			mp.start();
		} else {
			mp.pause();
		}
	}

	@Override
	public void writeBestScore() {
		// TODO Auto-generated method stub
		if (numCount < bestClick) {
			bestClick = numCount;
		}
		if (100 - numTime < bestTime) {
			bestTime = 100 - numTime;
		}
		stateStatus = true;
		edit.putInt("besttime", bestTime);
		edit.putInt("bestclick", bestClick);
		edit.putBoolean("statestatus", stateStatus);
		edit.commit();
	}

	@Override
	public void readBestScore() {
		// TODO Auto-generated method stub
		stringShared = this.getSharedPreferences(FILE_NAME, MODE_APPEND);
		edit = stringShared.edit();
		bestTime = stringShared.getInt("besttime", 999);
		bestClick = stringShared.getInt("bestclick", 999);
		stateStatus = stringShared.getBoolean("statestatus", false);
		stateStatus2 = stateStatus;
	}

}
